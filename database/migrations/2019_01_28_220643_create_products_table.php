<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id')->comment('Identification of Product.');
            $table->string('lm')->comment('LM identification.');
            $table->string('name')->comment('Product name.');
            $table->boolean('free_shipping')->comment('Verify if product have free shipping.');
            $table->text('description')->comment('Product description.');
            $table->decimal('price', 8, 2)->comment('Product price.');
            $table->dateTime('created')->comment('Time the row is created.');
            $table->dateTime('updated')->nullable()->comment('Time this row is updated.');
            $table->dateTime('deleted')->nullable()->comment('Time this row was deleted.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
