<?php

use Faker\Generator as Faker;
use App\Models\Product;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'lm' => $faker->randomNumber(4),
        'name' => $faker->word,
        'free_shipping' => $faker->boolean,
        'description' => $faker->paragraph,
        'price' => $faker->randomFloat(2, 1, 4),
    ];
});
