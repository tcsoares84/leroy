## TESTE

Teste utilizando Docker, PHP 7.3, Laravel 5.7 e MySQL 5.7. 

### Instalação

Faça download ou clone via repositório Git.

```sh
$ cd /path/to/install/
$ git clone git@gitlab.com:tcsoares84/leroy.git
```

* Crie uma cópia do arquivo .env para gerenciamento de configurações:

```sh
$ cd leroy/
$ cp .env.example .env
```

* Se estiver utilizando Git executar o comando para o Git ignorar mudanças de permissões (chmod) em arquivos:

```sh
$ git config core.fileMode false
```

* Configure as permissões de owner e group para os diretórios /storage e /bootstrap.

```sh
$ sudo chgrp -R www-data storage bootstrap/cache
$ sudo chmod -R ug+rwx storage bootstrap/cache
```

* Para rodar a aplicação é necessário o uso da porta 9000 e Docker para gerenciar os containners da aplicação [Docker](https://www.docker.com/) se nao tiver instalado favor modificar as configurações de banco para local e as .

```sh
$ cd leroy/env/
$ docker-compose up -d
```

* Entre no containner do PHP para instalar as dependências com [Composer](https://getcomposer.org/doc/):

```sh
$ docker exec -it php-fpm bash
```

* Para instalar as dependências do PHP:

```sh
$ composer install --ignore-platform-reqs
```

* Configure a chave de segurança da aplicação:

```sh
$ php artisan key:generate
```

* E necessário alterar as configurações do MySQL no arquivo .env, para usar o Gateway padrão do containner altere a chave 'DB_HOST' com o valor '127.0.0.1' para o IP do containner:

```sh
$ docker inspect mysql | grep "Gateway"
```

* Rode a migration para criar a tabela no MySQL:

```sh
$ php artisan migrate
```

* Forçar sincronização de dados da planilha com o banco de dados:

```sh
$ php artisan spreadsheet:import products_teste_webdev_leroy.xlsx
```

* Caso precise de outra planilha colocar no diretório publico e trocar nome do arquivo (XLSX ou XLS):
```sh
$ php artisan spreadsheet:import NOMEDOARQUIVO.xls
```

* Para ver funcionando teste:

[http://localhost:9000/api/](http://localhost:9000/api/)

* Para rodar os testes com PHPUnit:
```sh
$ ./vendor/bin/phpunit --debug
```

### API
| METODO | ENDPOINT | FUNCIONALIDADE |
| ------ | ------ | ------ |
| GET | http://localhost:9000/api/products | LISTA TODOS OS PRODUTOS |
| GET | http://localhost:9000/api/products/{id} | LISTA UM PRODUTO |
| PUT, PATCH | http://localhost:9000/api/products/{id} | ATUALIZA UM PRODUTO |
| DELETE | http://localhost:9000/api/products/{id} | DELETA UM PRODUTO |

=(^.^)=
