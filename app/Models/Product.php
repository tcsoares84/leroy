<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
    const DELETED_AT = 'deleted';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lm',
        'name',
        'free_shipping',
        'description',
        'price'
    ];

    /**
     * The attributes to be hidden for results.
     *
     * @var array
     */
    protected $hidden = [
        'created',
        'updated',
        'deleted'
    ];
}