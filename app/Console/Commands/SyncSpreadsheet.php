<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ImportService;

class SyncSpreadsheet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spreadsheet:import {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import spreadsheet data to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ImportService $import
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function handle(ImportService $import)
    {
        $filename = $this->argument('filename');
        $extension  = last(explode('.', $filename));

        if (!in_array($extension, ['xlsx', 'xls'])) {
            return $this->warn('Error! Not supported extension to be imported!');
        }

        if (!is_file(public_path('files/spreadsheets/') . $filename)) {
            return $this->warn('Error! This file is not exist to be imported!');
        }

        if ($import->importData($filename)) {
            return $this->alert('Spreadsheet update database data successfully!');
        }

        return $this->warn('Sorry, database is not updated!');
    }
}
