<?php declare(strict_types=1);

namespace App\Services;

use Exception;
use http\Exception\InvalidArgumentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Eloquent\ProductRepository;
use App\Exceptions\CustomErrorException;
use App\Exceptions\CustomNotFoundProductException;

class ProductService extends Service
{
    /**
     * Default property for product repository.
     *
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * Set default instance for product repository.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * List all items from collection.
     *
     * @return array
     * @throws CustomErrorException
     */
    public function getAllProducts() : array
    {
        $entities = $this->productRepository->all();

        return [
            'code' => 200,
            'message' => 'OK',
            'total' => count($entities->toArray()),
            'data' => $entities->toArray()
        ];
    }

    /**
     * List a specific item from collection.
     *
     * @param int $id
     * @return array
     * @throws CustomNotFoundProductException
     */
    public function findProductById(int $id) : array
    {
        $entity = $this->productRepository->find($id);

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => $entity
        ];
    }

    /**
     * Update specific item from collection.
     *
     * @param int $id
     * @param array $data
     * @return array
     * @throws CustomErrorException
     * @throws CustomErrorException
     * @throws CustomNotFoundProductException
     */
    public function updateProductById(int $id, array $data) : array
    {
        $this->productRepository->update($id, $data);

        return [
            'code' => 200,
            'message' => 'OK',
        ];
    }

    /**
     * Delete specific item from collection.
     *
     * @param int $id
     * @return array
     * @throws CustomErrorException
     * @throws CustomNotFoundProductException
     */
    public function deleteProductById(int $id) : array
    {
        $this->productRepository->delete($id);

        return [
            'code' => 200,
            'message' => 'OK',
        ];
    }
}