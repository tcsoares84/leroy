<?php declare(strict_types=1);

namespace App\Services;

use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Repositories\Eloquent\ImportRepository;
use App\Repositories\Eloquent\ProductRepository;

class ImportService extends Service
{
    /**
     * Default property for product repository.
     *
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * Default property for import repository.
     *
     * @var ImportRepository
     */
    protected $importRepository;

    /**
     * Default name of spreadsheet to be imported.
     *
     * @var $filename
     */
    protected $filename = 'products_teste_webdev_leroy.xlsx';

    /**
     * Set default instance for product repository.
     *
     * @param ProductRepository $productRepository
     * @param ImportRepository $importRepository
     */
    public function __construct(ProductRepository $productRepository, ImportRepository $importRepository)
    {
        $this->productRepository = $productRepository;
        $this->importRepository = $importRepository;
    }

    /**
     * Read spreadsheet titles for cells.
     *
     * @param string $filename
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    protected function readSpreadsheetTitles(string $filename) : array
    {
        $sheet = $this->loadSpreadsheet($filename);
        $rows = $sheet->getRowIterator(3, 3);
        $values = [];

        foreach ($rows as $row) {
            $cells = $row->getCellIterator('A', 'E');
            foreach ($cells as $cell) {
                $values[] = $cell->getValue();
            }
        }

        return $values;
    }

    /**
     * Read spreadsheet data of cells.
     *
     * @param string $filename
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    protected function readSpreadsheetData(string $filename) : array
    {
        $sheet = $this->loadSpreadsheet($filename);
        $rows = $sheet->getRowIterator(4);

        $values = [];
        foreach ($rows as $row) {
            $cells = $row->getCellIterator('A', 'E');
            foreach ($cells as $cell) {
                $values[] = $cell->getValue();
            }
        }

        return $values;
    }

    /**
     * Load specific spreadsheet file.
     *
     * @param string $filename
     * @return object
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    protected function loadSpreadsheet(string $filename) : object
    {
        $fullPathFile = public_path('files/spreadsheets/') . $filename;
        $spreadsheet = IOFactory::load($fullPathFile);
        $sheet = $spreadsheet->getActiveSheet();

        return $sheet;
    }

    /**
     * Read spreadsheet data and mounts the  array correctly.
     *
     * @param array $titles
     * @param array $values
     * @return array
     */
    protected function formatDataToArray(array $titles, array $values) : array
    {
        $delimiter = count($titles);
        $counter = 0;
        $row = $items = [];

        foreach ($values as $key => $value) {
            if ($counter < $delimiter) {
                $row[$titles[$counter]] = $value;
                $counter++;
            }

            if ($counter === $delimiter) {
                $items[] = $row;
                $counter = 0;
            }
        }

        return $items;
    }

    /**
     * Read spreadsheet titles and data to be sent to mount final array.
     *
     * @param string $filename
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function getSpreadsheetData(string $filename) : array
    {
        $titles = $this->readSpreadsheetTitles($filename);
        $values = $this->readSpreadsheetData($filename);

        return $this->formatDataToArray($titles, $values);
    }

    /**
     * Count elements of spreadsheet.
     *
     * @return int
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function getSpreadsheetElementsCount() : int
    {
        return count ($this->getSpreadsheetData());
    }

    /**
     * Import spreadsheet data to database.
     *
     * @param string $filename
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function importData(string $filename) : bool
    {
        $data = $this->getSpreadsheetData($filename);

        return $this->productRepository->massImport($data);
    }

    /**
     * List all import details.
     *
     * @return array
     */
    public function getImportsDetails() : array
    {
        $data = $this->importRepository->readImportDetails();

        return [
            'code' => 200,
            'message' => 'OK',
            'total' => count($data),
            'data' => $data
        ];
    }
}