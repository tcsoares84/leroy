<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\ImportService;

class ProductController extends Controller
{
    /**
     * Default property for product service.
     *
     * @var ProductService
     */
    public $productService;

    /**
     * Default property for import service.
     *
     * @var ImportService
     */
    public $importService;

    /**
     * Set a default instance for service.
     *
     * @param ProductService $productService
     * @param ImportService $importService
     */
    public function __construct(ProductService $productService, ImportService $importService)
    {
        $this->productService = $productService;
        $this->importService = $importService;
    }

    /**
     * List all products.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CustomErrorException
     */
    public function all() : object
    {
        $response = $this->productService->getAllProducts();

        return response()->json($response, $response['code']);
    }

    /**
     *  List specific product.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CustomNotFoundProductException
     * @throws \App\Exceptions\CustomInvalidArgumentException
     */
    public function find(int $id) : object
    {
        $response = $this->productService->findProductById($id);

        return response()->json($response, $response['code']);
    }

    /**
     *  Update specific product.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CustomErrorException
     * @throws \App\Exceptions\CustomInvalidArgumentException
     * @throws \App\Exceptions\CustomNotFoundProductException
     */
    public function update(Request $request, int $id) : object
    {
        $data = $request->all();
        $response = $this->productService->updateProductById($id, $data);

        return response()->json($response, $response['code']);
    }

    /**
     * Delete specific product.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CustomErrorException
     * @throws \App\Exceptions\CustomInvalidArgumentException
     * @throws \App\Exceptions\CustomNotFoundProductException
     */
    public function delete(int $id) : object
    {
        $response = $this->productService->deleteProductById($id);

        return response()->json($response, $response['code']);
    }
}