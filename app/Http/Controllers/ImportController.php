<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\ImportService;

class ImportController extends Controller
{
    /**
     * Default property for import service.
     *
     * @var ImportService
     */
    public $importService;

    /**
     * Set a default instance for service.
     *
     * @param ImportService $importService
     */
    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    /**
     * List spreadsheets imports details.
     *
     * @return object
     */
    public function spreadsheets() : object
    {
        $response = $this->importService->getImportsDetails();

        return response()->json($response, $response['code']);
    }
}