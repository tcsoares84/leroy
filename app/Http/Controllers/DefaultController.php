<?php declare(strict_types=1);

namespace App\Http\Controllers;


class DefaultController extends Controller
{
    /**
     * Return test API json.
     *
     * @return object
     */
    public function default() : object
    {
        return response()->json([
            'code' => 200,
            'message' => 'OK'
        ]);
    }
}