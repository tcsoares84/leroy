<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Repository;
use App\Models\Import;

class ImportRepository extends Repository
{
    /**
     * Default instance for import model.
     *
     * @var Import
     */
    protected $importModel;


    /**
     * Set default instance for import model.
     *
     * @param Import $importModel
     */
    public function __construct(Import $importModel)
    {
        $this->importModel = $importModel;
    }

    /**
     * Save the import details.
     *
     * @param array $data
     * @return mixed
     */
    public function saveImport(array $data)
    {
        return $this->importModel->create($data);
    }

    /**
     * List all import details.
     *
     * @param array $columns
     * @return array
     */
    public function readImportDetails(array $columns = ['*']) : array
    {
        return $this->importModel->get($columns)->toArray();
    }
}