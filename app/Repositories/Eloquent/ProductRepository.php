<?php

namespace App\Repositories\Eloquent;

use App\Exceptions\CustomErrorException;
use App\Exceptions\CustomNotFoundProductException;
use Exception;
use ErrorException;
use App\Repositories\Repository;
use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class ProductRepository extends Repository
{
    /**
     * Default instance for product model.
     *
     * @var Product
     */
    protected $productModel;

    /**
     * Set default instance for product model.
     *
     * @param Product $productModel
     */
    public function __construct(Product $productModel)
    {
        $this->productModel = $productModel;
    }

    /**
     * List all items from collection.
     *
     * @param array $columns
     * @return object
     * @throws CustomErrorException
     */
    public function all(array $columns = ['*']) : object
    {
        try {
            $entities = $this->productModel->get($columns);

            if (!$entities) {
                throw new Exception('An error occurring while try to read database data!');
            }

            return $entities;
        } catch (Exception $exception) {
            throw new CustomErrorException($exception->getMessage());
        }
    }

    /**
     * Create a new collection item.
     *
     * @param array $data
     * @return object|null
     * @throws CustomErrorException
     */
    public function create(array $data) : ?object
    {
        try {
            if (0 == count($data)) {
                throw new Exception('Content must be array and is not valid array or empty!');
            }
        } catch (Exception $exception) {
            throw new CustomErrorException($exception->getMessage());
        }

        try {
            return $this->productModel->create($data);
        } catch (QueryException $exception) {
            throw new CustomErrorException($exception->getMessage());
        }
    }

    /**
     * List specific item from collection.
     *
     * @param int $id
     * @param array $columns
     * @return object|null
     * @throws CustomNotFoundProductException
     */
    public function find(int $id, array $columns = ['*']) : ?object
    {
        try {
            $entity = $this->productModel->find($id, $columns);

            if (!$entity) {
                throw new Exception('Product not found!');
            }

            return $entity;
        } catch (ModelNotFoundException | ErrorException | Exception $exception) {
            throw new CustomNotFoundProductException($exception->getMessage());
        }
    }

    public function findProductByLM(string $lm)
    {

    }

    /**
     * Update specific item from collection.
     *
     * @param int $id
     * @param array $data
     * @return bool|null
     * @throws CustomErrorException
     * @throws CustomNotFoundProductException
     */
    public function update(int $id, array $data) : ?bool
    {
        try {
            $validProductData = $this->filterValidData($data);

            if (count($validProductData) < 1) {
                throw new Exception('The values ​​that were sent in the request can not be updated or do not exist!');
            }
        } catch (Exception | ErrorException $exception) {
            throw new CustomErrorException($exception->getMessage());
        }

        try {
            $entity = $this->find($id);

            if (!$entity) {
                throw new ModelNotFoundException('Product not found!');
            }
        } catch (ModelNotFoundException $exception) {
            throw new CustomNotFoundProductException($exception->getMessage());
        }

        try {
            return $this->productModel->where('id', '=', $id)
                ->update($validProductData);
        } catch (Exception | QueryException $exception) {
            throw new CustomErrorException($exception->getMessage());
        }
    }

    /**
     * Delete specific item from collection.
     *
     * @param int $id
     * @return bool|null
     * @throws CustomErrorException
     * @throws CustomNotFoundProductException
     */
    public function delete(int $id) : bool
    {
        try {
            $entity = $this->find($id);

            if (!$entity) {
                throw new ModelNotFoundException('Product not found!');
            }
        } catch (ModelNotFoundException $exception) {
            throw new CustomNotFoundProductException($exception->getMessage());
        }

        try {
            return $this->productModel->destroy($id);
        } catch (Exception | QueryException $exception) {
            throw new CustomErrorException($exception->getMessage());
        }
    }

    /**
     * Import mass items for database.
     *
     * @param array $data
     * @return bool
     */
    public function massImport(array $data) : ?bool
    {
        $valid = [];

        foreach ($data as $key => $item) {
            $valid[$key] = $this->filterValidData($item);
        }

        return $this->multipleInserts($valid);
    }

    /**
     * Create multiple inserts or updates into collection.
     *
     * @param array $data
     * @return bool
     */
    protected function multipleInserts(array $data) : bool
    {
        try {
            foreach ($data as $item) {
                $this->create($item);
            }

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * Filter data for remove invalid fields.
     *
     * @param array $data
     * @return array
     */
    public function filterValidData(array $data) : array
    {
        $allowedTypes = ['lm', 'name', 'free_shipping', 'description', 'price'];

        $valid = [];
        foreach ($data as $key => $value) {
            if (in_array($key, $allowedTypes)) {
                $valid[$key] = $data[$key];
            }
        }

        return $valid;
    }
}