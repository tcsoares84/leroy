<?php

namespace App\Exceptions;

use Exception;

class CustomNotFoundProductException extends Exception
{
    /**
     * Render response for request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json([
            'code' => 404,
            'message' => $this->message,
        ], 404);
    }
}
