<?php

namespace App\Exceptions;

use Exception;

class CustomErrorException extends Exception
{
    /**
     * Render response for request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json([
            'code' => 400,
            'message' => 'Bad request!',
            'error' => $this->message,
        ], 400);
    }
}
