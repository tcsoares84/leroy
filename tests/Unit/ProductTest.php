<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Product;
use App\Repositories\Eloquent\ProductRepository;
use App\Exceptions\CustomErrorException;
use App\Exceptions\CustomNotFoundProductException;

class ProductTest extends TestCase
{
    public $productData;

    public function testIfProductCanNotBeCreatedBecauseIsEmpty()
    {
        $this->expectException(CustomErrorException::class);
        $productRepository = new ProductRepository(new Product());
        $productRepository->create([]);
    }

    public function testIfProductCanBeCreated()
    {
        $productData = factory(Product::class)->make();
        $productRepository = new ProductRepository(new Product());
        $product = $productRepository->create($productData->toArray());

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($productData->lm, $product->lm);
        $this->assertEquals($productData->name, $product->name);
        $this->assertEquals($productData->free_shipping, $product->free_shipping);
        $this->assertEquals($productData->description, $product->description);
        $this->assertEquals($productData->price, $product->price);
    }

    public function testIfProductCanNotBeReadBecauseHaveInvalidId()
    {
        $this->expectException(CustomNotFoundProductException::class);
        $productRepository = new ProductRepository(new Product());
        $productRepository->find(666);
    }

    public function testIfProductCanBeRead()
    {
        $productRepository = new ProductRepository(new Product());
        $product = $productRepository->find($this->productData->id);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($product->lm, $this->productData->lm);
        $this->assertEquals($product->name, $this->productData->name);
        $this->assertEquals($product->free_shipping, $this->productData->free_shipping);
        $this->assertEquals($product->description, $this->productData->description);
        $this->assertEquals($product->price, $this->productData->price);
    }

    public function testIfProductCanNotBeUpdateBecauseHaveInvalidArguments()
    {
        $data = [
            'name' => null,
        ];

        $this->expectException(CustomErrorException::class);
        $productRepository = new ProductRepository(new Product());

        $productRepository->update($this->productData->id, $data);
    }

    public function testIfProductCanBeUpdated()
    {
        $data = [
            'name' => 'Test PHPUnit',
            'price' => 0.00
        ];

        $productRepository = new ProductRepository(new Product());
        $update = $productRepository->update($this->productData->id, $data);
        $product = $productRepository->find($this->productData->id);

        $this->assertTrue($update);
        $this->assertEquals($product->lm, $this->productData->lm);
        $this->assertEquals($product->name, $data['name']);
        $this->assertEquals($product->free_shipping, $this->productData->free_shipping);
        $this->assertEquals($product->description, $this->productData->description);
        $this->assertEquals($product->price, $data['price']);
    }
//
//    public function testIfProductCanNotBeUpdateBecauseHaveInvalidId()
//    {
//        $data = [
//            'name' => 'Test PHPUnit2',
//        ];
//
//        $this->expectException(CustomNotFoundProductException::class);
//        $productRepository = new ProductRepository(new Product());
//
//        $productRepository->update($this->productData->id + 7, $data);
//    }
//
//    public function testIfProductCanNotBeDeletedBecauseHaveInvalidId()
//    {
//        $this->expectException(CustomNotFoundProductException::class);
//        $productRepository = new ProductRepository(new Product());
//        $delete = $productRepository->delete($this->productData->id + 7);
//
//        $this->assertNull($delete);
//    }
//
//    public function testIfProductCanBeDeleted()
//    {
//        $productRepository = new ProductRepository(new Product());
//        $delete = $productRepository->delete($this->productData->id);
//        $this->assertTrue($delete);
//    }
}
