<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'DefaultController@default');

Route::group([
    'prefix' => 'products'
], function ($router) {
    Route::get('/', 'ProductController@all');
    Route::get('/{id}', 'ProductController@find');
    Route::match(['put', 'patch'], '/{id}', 'ProductController@update');
    Route::delete('/{id}', 'ProductController@delete');
});

Route::group([
    'prefix' => 'imports'
], function ($router) {
    Route::get('/spreadsheets', 'ImportController@spreadsheets');
});
